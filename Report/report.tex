\documentclass[a4paper]{article} \usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc} \usepackage{lmodern}
\usepackage[english]{babel} \usepackage[margin=1in]{geometry}
\usepackage{hyperref}
\usepackage{floatrow}

\usepackage{listings} %The code formatting Library
\usepackage{color}

\usepackage{graphicx}
\DeclareGraphicsExtensions{.pdf,.png,.jpg,.JPG}
\graphicspath{ {Images} }

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}


\definecolor{codegreen}{rgb}{0,0.4,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\scriptsize,
    breakatwhitespace=false,
    breaklines=true,
    captionpos=b,
    keepspaces=true,
    numbers=left,
    numbersep=5pt,
    showspaces=false,
    showstringspaces=false,
    showtabs=false,
    tabsize=4,
    frame=single
}

\lstset{style=mystyle}


\begin{document}
\begin{center}
\textsc{ University of Canterbury, ENCE463}\\[0.4cm]
\HRule \\[0.4cm]
{ \huge \bfseries Directional Ultrasound Sensing Review \\[0.4cm] }
Using FreeRTOS and the Stellaris LM3S1968\\[0.4cm] \HRule \\[0.5cm]

Geoffrey Irons (34591492)\\
{\large \today}
\end{center}

\section{Introduction}
Ultrasound sensors are cheap and readily available from a number of sources.
All commercially available ultrasound sensors are one-dimensional pickups,
reporting the distance between the sensor and the target. The aim of this
project is to develop the processor for an ultrasound sensor able to
determine two dimensions: distance and angle.

\section{Implementation}
\subsection{Hardware}
For this project, two HC-SR04 ultrasound modules were used to provide the analog
electronics, and the processing was performed using FreeRTOS on the Stellaris
LS3M1968 evaluation board. Instead of using the digital outputs from the
ultrasound modules they were modified to provide analog inputs to the Stellaris's
ADC channels. An overview of the system and the finished unit is
visible in Figure \ref{fig:hardware}

\begin{figure}[h]
    \begin{minipage}[b]{0.4\textwidth}
        \centering
        \includegraphics[width=0.8\textwidth]{Images/systemlevel.jpg}
        \caption{An overview of the system hardware
        \label{fig:hardware}}
    \end{minipage}
    \begin{minipage}[b]{0.4\textwidth}
        \centering
        \includegraphics[width=0.8\textwidth]{Images/hardware.jpg}
    \end{minipage}
\end{figure}

\subsection{Software}
The software was designed around the "simplest modular solution" approach.
Effort was put into making the system extensible and modular, but not to the
point of wasting effort abstracting things totally. For example, the interface
between the OLED module and the rest of the program was abstracted to placing
text in a specified location on the screen through a queue, but the interface
between the OLED module and the actual hardware was not abstracted at all,
making direct calls to the system library. In the case of a prototype unit
this approach promotes rapid development while allowing feature-wise
extensibility.

\subsubsection{Data Acquisition}
For this project, the ultrasound waveforms need to be sampled extremely
rapidly. This was handled using the ADC, it's sample sequencers and a PWM
generator. The ADC was set to sample at 1MSPS conversion rate, and to sample
both the left and right channel on each trigger. The PWM generator was
configured to sample the channels at 200Khz.
Upon quantization, the 10-bits of resolution of the ADC was downscale into
8-bits to allow more samples to be stored in SRAM.

To control when the sampling occurred a semaphore was used. A FreeRTOS task
was in a blocking state waiting for a semaphore. Upon being received the PWM
generator was started. Inside the ADC interupt, when the buffer was full, the
PWM generator was stopped.

\subsubsection{Data Communication}
Due to the 200Khz sample frequency of the ADC's, FreeRTOS's queue
implementation was unable to send a message in 5 microseconds. After initial
experimentation using queues, the use of a queue was dropped entirely in
favour of a shared global buffer.

Sharing global data is risky so protection was added as a global
write-pointer. This is a 16-bit integer that stores where
the buffer is being written to. From a module reading the data the location
of the write-pointer is checked before accessing the data buffer. Only after
a write does the ADC interupt increment the write pointer, thus ensuring that
the data accessible by the rest of the program has been serviced.
Protection of the write pointer is not necessary because:
\begin{enumerate}
\itemsep0em
\item It is 16-bit and thus atomic to write on the stellaris
\item It is being written to from an ISR and there are no higher priority interupts.
\end{enumerate}
Resetting the write-pointer was done by stopping the ADC interrupt, and from
setting the write-pointer to zero from within freeRTOS.

Communication between the ultrasoud processing module and the display was done
through two queues. Because this only needed to occur when data was available,
the slower speed of the freeRTOS queue was acceptable as they simplify the
interface between modules. The complete data flow and means of communication
can be seen in \ref{fig:dataflow}.

\begin{figure}[hb]
    \centering
    \includegraphics[width=1.0\textwidth]{Images/flow.png}
    \caption{Data flow through the program
    \label{fig:dataflow}}
\end{figure}

\subsection{Signal Processing}
Whilst this is not a signal processing paper, significant time was
invested in signal processing to achieve the specified accuracy of $\pm$1cm.
Detection was performed by comparing the value to a threshold. Whenever the
signal exceeded a value in the positive or negative direction, it was
assumed a reflection was detected. In theory this should allow up to 0.13$\mu$S
accuracy (half a wave) however due to the resonant characteristics of the
transducers this was not the case.

A further complication was electrical noise between the transmitting and
receiving circuit creating a slight waveform several microseconds after
triggering. This was dealt with by changing the threshold value linearly
after triggering. The threshold shape can be seen in
\ref{fig:signalprocessing}. The first sample in the red shaded area was
marked as being the closest target.
\begin{figure}[hb]
    \centering
    \includegraphics[width=0.8\textwidth]{Images/ElectricalNoise.png}
    \caption{From left to right: electrical noise, target at 15cm, detection envelope
    \label{fig:signalprocessing}}
\end{figure}


The end performance of the signal processing was a distance accuracy of
$\pm$1cm, an angle accuracy of $\pm$10 degrees and a detection range of
approximately two meters for large targets. This is not a limitation
of the hardware or the sampling. The sampling at 200Khz with 8-bit resolution
is sufficient for significantly higher performance, but time limitations
prevented these from being attained.


\section{Tasks and Modules}
The code has been structured in a logical way, such that each module does
a single job. The interfaces between modules have been harshly defined to
improve coupling.
The dependency graph of project is visible in Figure \ref{fig:dependencies}.
A full graph of the includes is visible (for humour value) in Appendix \ref{app:deps}

\begin{figure}[hb]
    \centering
    \includegraphics[width=0.6\textwidth]{Images/dependencies.png}
    \caption{A subsection of the include graph showing dependencies. Note that FreeRTOS dependencies have been removed.
    \label{fig:dependencies}}
\end{figure}

Each module was as independent as possible from other others, being a black box
as much as possible. Even the initialization of each module was as a single
function call that would set up the hardware, create queues/semaphores and
schedule the tasks with FreeRTOS.
In total there were five tasks:
\begin{enumerate}
\itemsep0em
\item OLEDgraph - Updates the graph of the target position
\item OLEDtext - Displays a line of text somewhere on the display
\item UltrasoundIn - Manages the ADC peripheral (starting it running based on a semaphore)
\item UltrasoundProcessor - Processes the raw data to identify the location of a target
\item UserInput - Handles button presses and the different 'modes' of operation as per the original spec.
\end{enumerate}
More details about these tasks are in their respective sections below.
Note that the code snippets are the entire header files with the exception
of preprocessing directives. (eg \#ifndef)

\subsection{OLED}
The OLED module handles all interfacing with the OLED hardware. It provides
two externally accessible queues. One is a text message, allowing external
tasks to write strings to the screen in a specified position. During
development, this allowed easy debugging without requiring UART. The other
queue (and message type) was used to place the marker on the graph. The use of
queues allowing thread-safe operation from any number of tasks.

Inside the OLED module two tasks were presentm one handling each queue. To
avoid both writing to the hardware at the same time, a binary semaphore was used
to block the screen when a task was writing. This was only accessible
internally to the module.

The OLED module depends on the Luminary libraries "rit128x96x4.h" module for
accessing the actual OLED display. It also depends on the FreeRTOS queue and
semaphore modules to create the modules input queues and module. The
interfaces the OLED module provides can be seen in the header file snippet
below:
\lstinputlisting[language=C, firstline=7, lastline=23, label=lst:OLEDInterface]{../Code/oled.h}


\subsection{UltrasoundIn}
The ultrasound input module handles the conversion of the analog voltage
into a buffer that can be processed. This module contains the "UltrasoundIn"
task, but the bulk of it's job is configuring and controlling the PWM generator
used to trigger the ADC readings, and an interrupt that runs when the ADC
conversion has completed.

\lstinputlisting[language=C, firstline=8, lastline=11, label=lst:UltrasoundIn]{../Code/ultrasoundin.h}
The dependencies of the ultrasound in module are the most complex of the
modules. It depends on the "adc.h" and "pwm.h" libraries to access the
hardware. It also requires some external variables to write into, namely:
\begin{itemize}
\itemsep0em
\item uint8\_t leftRawBuffer[TOTAL\_BUFFER\_SIZE] to store the data from the left channel ADC
\item uint8\_t rightRawBuffer[TOTAL\_BUFFER\_SIZE] to store the data from the right channel ADC
\item uint16\_t currentWriteLocation to store the position it is most recently wrote to. This prevents the UltrasoundProcessor task from reading invalid data
\end{itemize}
Using global variables to communicate the data was necessary due to the high
data bandwidth. In 0.02 seconds 2.4kB of data is generated, and methods other
than directly writing into buffers (eg queues) proved to be unfeasible.
These buffers are contained in globals.h.

\subsection{UltrasoundProcessor}
The ultrasound processor takes input from the raw buffers from the ADC and
converts it to a distance and angle. As such, this module depends on the
buffers and write pointer in globals.h. To output it's information it
also depends on the OLED module, both the graph and text queue.
\lstinputlisting[language=C, firstline=4, lastline=5, label=lst:UltrasoundProcessor]{../Code/ultrasoundprocessor.h}

\subsection{UserInput}
The user input module handles any user input interaction, such as the
nav-buttons. From here it controls various aspects of the program, such as
giving the semaphore to trigger the ultrasound conversion (triggerUltrasoundIn).
In line with the requirements there are two modes for activation: continuous and
single shot. These modes are internal to the UserInput module.

Because this is an input module the only externally accessible function
is the initializer.
\lstinputlisting[language=C, firstline=5, lastline=5, label=lst:UserInput]{../Code/userinput.h}

\subsection{Globals}
This module contains any shared variables such as the buffers used by
ultrasoundin. Functions and clearly owned objects (such as queues and
semaphores) are owned by their respective modules.
The globals file provides the following variables:
\lstinputlisting[language=C, firstline=7, lastline=9, label=lst:globals.h]{../Code/globals.h}


\section{Testing}
The development of this project has been iterative, and significant testing has
been performed on each iteration. The test regime involved testing almost every
change. An effort was made to build the project in small steps allowing the
system to be tested at every stage. When under active development, a new version
of the code was pushed to the board and tested approximately every ten minutes.
As such, although there is little in the way of unit tests and very little
test scaffold, the system has been thoroughly tested. Such a rigorous test
regime caught many bugs before they could hide themselves in "assumed stable"
code. The system has been tested for durations of up to one hour, proving the system
is at stable in the medium term.

The use of a queue leading to the OLED hardware allowed the on-board screen to
be used for debugging purposes. This was required as initial attempts to use
the Stellaris' onboard USB to Serial FTDI converter was unsuccessful, meaning
that exporting debug information to the computer was a challenge. GDB was
used when investigating various lockups, which were normally a result of
forgetting to enable a peripheral.


\subsection{Timing}
The bulk of the processor time is spent idling. This is due to a hardware
limitation of the ultrasound modules. They are unable to be fired more than
four times per second, so for extended periods of time nothing can be done.
When the ultrasound modules are running and being sampled, 60 percent of
the processor time goes into servicing the ADC interupt. This can be seen
in Figure \ref{fig:sampling}.  To produce this graph, a GPIO was turned on at
the start of the interrupt and turned off at the end.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.6\textwidth]{Images/scope_0.png}
    \caption{the ADC interupt while sampling the ultrasound sensors. Bottom trace is the ADC interupt, top is the ultrasound waveform.
    \label{fig:sampling}}
\end{figure}

The other tasks (display, ultrasound processing and user input checking) are
all equal priority. Display and ultrasound processing are both blocking tasks,
spending a significant amount of time waiting for external events, while the
user input is polled and non-blocking. This setup results in all tasks being
processed when required, with the user input time-sharing when required or
occupying the processor when it is available - which reduces response time to
unnoticeable levels.

\section{Conclusions}
Using a FreeRTOS is a good idea when there are many soft-real-constraints, but
when there are hard real time constraints or only a single "task" a much
simpler scheduler will often suffice. In this project, the RTOS served
nicely to manage data going into the OLED screen, but it's use in the
high-bandwidth ultrasound processing was limited.

Initially the requirements were to create a system capable of sampling a 40Khz
wave from an ultrasound sensor at a minimum of 100Khz and to be able to
detect targets up to 4m away with an accuracy of $\pm$0.5cm and $\pm$10 degrees.
The end performance sampled the wave at 200Khz but only had a maximum range
of 2m. The distance accuracy was as required, but the angle resolution was
closer to $\pm$15 degrees.

Overall this project proved to be informative about the use of a real time
operating system in an embedded system, and while the sensor is not accurate
enough to be used in an actual application, a directional ultrasound sensor has
been prototyped.
\appendix
\section{\#include Tree}
\label{app:deps}
\includegraphics[angle=90, height=1\textheight]{Images/source.png}

\end{document}
