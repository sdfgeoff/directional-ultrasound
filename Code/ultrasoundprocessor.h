#ifndef ULTRSOUND_PROCESSOR_H
#define ULTRSOUND_PROCESSOR_H

void UltrasoundProcessorInit(void);   //Creates the task to process the ultrasound data
void resetUltrasoundProcessing(void); //Resets the processing for the next run

#endif

