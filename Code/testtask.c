#include <stdio.h>

#include "hw_memmap.h"
#include "hw_types.h"
#include "hw_sysctl.h"
#include "sysctl.h"
#include "gpio.h"

#include "oled.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#define TESTTASK_SEND_RATE ( ( portTickType ) 333 ) / portTICK_RATE_MS
#define TESETTASK_STACK_SIZE configMINIMAL_STACK_SIZE + 10
#define TESTTASK_PRIORITY tskIDLE_PRIORITY


void initStatusLed(void){
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOG);
    GPIOPinTypeGPIOOutput(GPIO_PORTG_BASE, GPIO_PIN_2);
}


void setStatusLedOn(void){
    GPIOPinWrite(GPIO_PORTG_BASE,
        (GPIO_PIN_2),
        0xFF);
}

void setStatusLedOff(void){
    GPIOPinWrite(GPIO_PORTG_BASE,
        (GPIO_PIN_2),
        0x00);
}


void TestTask(void *pvParameters){
    //This task sends a message to the screen every few seconds
    static OLEDTextMessage xMessage = { "'Ello" };

    while(1){
        vTaskDelay(TESTTASK_SEND_RATE / 2);
        xQueueSend(OLEDTextQueue, &xMessage, 1000);
        setStatusLedOn();

        vTaskDelay(TESTTASK_SEND_RATE / 2);
        setStatusLedOff();
    }
}

void TestTaskInit(void){
    xTaskCreate( TestTask, ( signed char * ) "TestTask", TESETTASK_STACK_SIZE, NULL, TESTTASK_PRIORITY, ( xTaskHandle * ) NULL );
    initStatusLed();
}
