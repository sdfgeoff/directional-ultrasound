#ifndef OLED_H
#define OLED_H

#include "FreeRTOS.h"
#include "queue.h"

#define OLED_MAX_MSG_LEN 25 //Max length string to display on the OLED

xQueueHandle OLEDTextQueue;    //The queue used to send text to the OLED
xQueueHandle OLEDGraphQueue;    //The queue used to send text to the OLED

typedef struct {            //The message that can be sent to the OLED
    char* pcMessage;
    uint8_t lineNum;
    uint8_t xPos;
} OLEDTextMessage;

typedef struct {            //The message that can be sent to the OLED to draw a graph
    uint8_t xPos;
    int8_t yPos;
} OLEDGraphMessage;

void OLEDInit(void);        //Function used to initialize the OLED task and hardware
#endif
