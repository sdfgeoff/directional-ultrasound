

#include <stdio.h>
#include <math.h>

#include "hw_memmap.h"
#include "hw_types.h"
#include "hw_sysctl.h"
#include "sysctl.h"
#include "gpio.h"

#include "FreeRTOS.h"
#include "task.h"

#include "config.h"
#include "ultrasoundprocessor.h"
#include "oled.h"
#include "usbserial.h"
#include "globals.h"

#define PROCESSTASK_STACK_SIZE configMINIMAL_STACK_SIZE + 1000
#define PROCESSTASK_PRIORITY tskIDLE_PRIORITY
#define PROCESSDATA_QUEUE_SIZE 100


uint32_t dont_trash_my_variables[10] = {0}; //Something is overflowing somewhere
uint16_t currentReadLocation = 0;
uint16_t left_peak_loc = 0;
uint16_t right_peak_loc = 0;
uint16_t left_peak_distance = 0;
uint16_t right_peak_distance = 0;
float distance = 0;
float angle = 0;
uint16_t moving_threshold = 2560;



static uint16_t samples_to_distance(uint16_t dist){
    //Converts the sample number into a distance in mm
    if (dist <= 150){
        return 0;
    }
    uint16_t new_dist = dist;
    new_dist = new_dist * 173 / 100;
    new_dist = dist - 140;
    return new_dist;
}

uint8_t checkThresholds(uint8_t val, uint16_t threshold){
    int16_t delta = (int16_t)val - (int16_t)DC_OFFSET;
    if (delta < -(int16_t)threshold/10 || delta > (int16_t)threshold/10){
        return 1;
    } else {
        return 0;
    }
}

static void processTask(void *pvParameters){
    static OLEDTextMessage xMessage = { "ERROR" , 9, 0};
    static OLEDGraphMessage graphMessage = { 0 , 0};
    char to_send[100] = {'\0'};

    while(1){
        currentReadLocation += 1;
        while (currentReadLocation >= currentWriteLocation){
            //Can't process samples as all available are already processed, so
            //wait for a little while. This also catches the end of the buffer
            //As currentWriteLocation stops incrementing
            vTaskDelay(10);
        }

        if (moving_threshold > END_SENSITIVITY){
            moving_threshold -= SENSITIVITY_DECAY_RATE;
        } else {
            moving_threshold = END_SENSITIVITY;
        }

        if (checkThresholds(leftRawBuffer[currentReadLocation], moving_threshold) != 0){
            if (left_peak_loc == 0){
                left_peak_loc = currentReadLocation;
            }
        }
        if (checkThresholds(rightRawBuffer[currentReadLocation], moving_threshold) != 0){
            if (right_peak_loc == 0){
                right_peak_loc = currentReadLocation;
            }
        }


        if (currentReadLocation == TOTAL_BUFFER_SIZE - 1){
            left_peak_distance = samples_to_distance(left_peak_loc);
            right_peak_distance = samples_to_distance(right_peak_loc);

            if (left_peak_loc == 0){
                if (right_peak_loc == 0){
                    sprintf(to_send, "D: ?   A:??  ");
                    graphMessage.xPos = 0;
                    graphMessage.yPos = 0;
                } else {
                    sprintf(to_send, "D:%4d A:-?  ", right_peak_distance);
                    graphMessage.xPos = distance;
                    graphMessage.yPos = -100;
                }
            } else if (right_peak_loc == 0){
                sprintf(to_send, "D:%4d  A:+?  ", left_peak_distance);
                graphMessage.xPos = distance;
                graphMessage.yPos = 100;
            } else {
                left_peak_distance += ANGLE_CORRECTION_FACTOR;
                distance = (float)(left_peak_distance + right_peak_distance) / 2.0;

                angle = - acos( (pow(RX_SEPERATION / 2, 2) + pow(distance, 2) - pow(left_peak_distance, 2)) / (float)(distance * RX_SEPERATION) );

                sprintf(to_send, "D:%4d A:%3d ", (int)distance, 90 + (int)(angle/3.14 * 180));
                graphMessage.xPos = distance / 10;
                graphMessage.yPos =  (int8_t)((distance * cos(angle)) / 5);

            }
            xMessage.pcMessage = to_send;
            xQueueSend(OLEDTextQueue, &xMessage, 0);
            xQueueSend(OLEDGraphQueue, &graphMessage, 0);
        }
    }
}

void resetUltrasoundProcessing(void){
    left_peak_loc = 0;
    right_peak_loc = 0;
    currentReadLocation = 0;
    moving_threshold = INITIAL_SENSITIVITY;
}

void UltrasoundProcessorInit(void){
    xTaskCreate( processTask, ( signed char * ) "ProcessTask", PROCESSTASK_STACK_SIZE, NULL, PROCESSTASK_PRIORITY, ( xTaskHandle * ) NULL );

}

