/* Standard includes. */
#include <stdio.h>

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* Hardware library includes. */
#include "hw_memmap.h"
#include "hw_types.h"
#include "hw_sysctl.h"
#include "sysctl.h"
#include "gpio.h"
#include "grlib.h"

/* Demo app includes. */
#include "oled.h"
#include "testtask.h"
#include "ultrasoundin.h"
#include "ultrasoundprocessor.h"
#include "userinput.h"
#include "usbserial.h"

/*-----------------------------------------------------------*/

// Configure hardware
static void prvSetupHardware( void );

/*-----------------------------------------------------------*/
int main( void )
{
    prvSetupHardware();
    initSerial(115200);

    OLEDInit();
    //TestTaskInit();
    UltrasoundInInit();
    UltrasoundProcessorInit();
    UserInputInit();

    /* Start the scheduler. */
    vTaskStartScheduler();

    while(1){
        /* Trap in case there is an error with freeRTOS (or stack overflow) */
    }
    return 0;
}
/*-----------------------------------------------------------*/

void prvSetupHardware( void ) {
    /* If running on Rev A2 silicon, turn the LDO voltage up to 2.75V.  This is
    a workaround to allow the PLL to operate reliably. */
    if( DEVICE_IS_REVA2 )
    {
        SysCtlLDOSet( SYSCTL_LDO_2_75V );
    }
    /* Set the clocking to run from the PLL at 50 MHz */
    SysCtlClockSet( SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_8MHZ );
}
/*-----------------------------------------------------------*/

void vApplicationTickHook( void ) {
    /* static xOLEDMessage xMessage = { "PASS" };
    static unsigned portLONG ulTicksSinceLastDisplay = 0;
    portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
    // Called from every tick interrupt.
    ulTicksSinceLastDisplay++;
    if( ulTicksSinceLastDisplay >= mainCHECK_DELAY ){
        ulTicksSinceLastDisplay = 0;
        xMessage.pcMessage = "I'm Running";
        xHigherPriorityTaskWoken = pdFALSE;
        xQueueSendFromISR( xOLEDQueue, &xMessage, &xHigherPriorityTaskWoken );
    } */
}

/*-----------------------------------------------------------*/


void vApplicationStackOverflowHook( xTaskHandle *pxTask, signed portCHAR *pcTaskName )
{
    while(1){
    }
}
