#include <stdio.h>

#include "rit128x96x4.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"


#include "oled.h"
#include "bitmap.h"

/* Constants used when writing strings to the display. */
#define OLED_CHARACTER_HEIGHT               ( 9 )
#define OLED_MAX_ROWS_96                    ( OLED_CHARACTER_HEIGHT * 10 )
#define OLED_FULL_SCALE                     ( 15 )
#define OLED_SSI_FREQ                       ( 3500000UL )
#define OLED_HEADER                         "ULTRASOUND DIRECTION"

// Configuration for the OLED queue
#define OLED_QUEUE_SIZE                     ( 3 )

// Task Configuration
#define OLED_TASK_STACK_SIZE                ( configMINIMAL_STACK_SIZE + 50 )
#define OLED_TASK_NAME                      "OLED"
#define OLED_PRIORITY                       tskIDLE_PRIORITY


xSemaphoreHandle writingToOLED;

/*-----------------------------------------------------------*/
void OLEDTextTask( void *pvParameters ){
    OLEDTextMessage xMessage;
    static portCHAR cMessage[ OLED_MAX_MSG_LEN ];

    //display a startup message.
    xSemaphoreTake( writingToOLED, portMAX_DELAY );
    RIT128x96x4StringDraw( OLED_HEADER, 0, 0, OLED_FULL_SCALE );
    xSemaphoreGive( writingToOLED );

    while(1){
        /* Wait for a message to arrive that requires displaying. */
        xQueueReceive( OLEDTextQueue, &xMessage, portMAX_DELAY );
        xSemaphoreTake( writingToOLED, portMAX_DELAY );
        sprintf( cMessage, "%s", xMessage.pcMessage );
        RIT128x96x4StringDraw( cMessage, xMessage.xPos, xMessage.lineNum * OLED_CHARACTER_HEIGHT, OLED_FULL_SCALE );
        xSemaphoreGive( writingToOLED );
    }
}

void OLEDGraphTask( void *pvParameters ) {
    OLEDGraphMessage graphMessage;
    uint8_t xPos = 0;
    int8_t yPos = 0;

    // Draw the background initially
    xSemaphoreTake( writingToOLED, portMAX_DELAY );
    RIT128x96x4ImageDraw( graphBitmap, 0, OLED_CHARACTER_HEIGHT*2 + 1, GRAPHWIDTH, GRAPHHEIGHT );
    xSemaphoreGive( writingToOLED );
    while(1){
        xQueueReceive( OLEDGraphQueue, &graphMessage, portMAX_DELAY );


        xPos = graphMessage.xPos;
        yPos = graphMessage.yPos;
        if (yPos > GRAPHHEIGHT / 2 - 2){
            yPos = GRAPHHEIGHT / 2 - 2;
        } else if (yPos < -GRAPHHEIGHT/2 + 2){
            yPos = -GRAPHHEIGHT/2 + 2;
        }
        if (xPos > GRAPHWIDTH){
            xPos = GRAPHWIDTH - 1;
        }
        yPos = yPos + GRAPHHEIGHT / 2 - MARKERHEIGHT / 2;
        xSemaphoreTake( writingToOLED, portMAX_DELAY );
        RIT128x96x4ImageDraw( graphBitmap, 0, OLED_CHARACTER_HEIGHT*2 + 1, GRAPHWIDTH, GRAPHHEIGHT );
        RIT128x96x4ImageDraw( markerBitmap, xPos, OLED_CHARACTER_HEIGHT*2 + 1 + yPos, MARKERWIDTH, MARKERHEIGHT );
        xSemaphoreGive( writingToOLED );

    }
}

void hardwareInit(void){
    RIT128x96x4Init( OLED_SSI_FREQ );
}


void OLEDInit(void){
    hardwareInit();
    OLEDTextQueue = xQueueCreate( OLED_QUEUE_SIZE, sizeof( OLEDTextMessage ) );
    OLEDGraphQueue = xQueueCreate( OLED_QUEUE_SIZE, sizeof( OLEDGraphMessage ) );
    vSemaphoreCreateBinary( writingToOLED );
    xTaskCreate( OLEDTextTask, ( signed portCHAR * ) OLED_TASK_NAME, OLED_TASK_STACK_SIZE, NULL, OLED_PRIORITY, NULL );
    xTaskCreate( OLEDGraphTask, ( signed portCHAR * ) "OLEDGraph", OLED_TASK_STACK_SIZE, NULL, OLED_PRIORITY, NULL );
}
