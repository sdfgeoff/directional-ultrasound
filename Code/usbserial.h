#ifndef USB_SERIAL_H
#define USB_SERIAL_H

void initSerial(uint32_t baud);
void sendString(char* outStr);

#endif
