#ifndef ULTRSOUND_IN_H
#define ULTRSOUND_IN_H

#include "FreeRTOS.h"
#include "semphr.h"


xSemaphoreHandle triggerUltrasoundIn; //Make true to start a conversion

void UltrasoundInInit(void);          // Creates the task for the ultrasound input.

#endif
