#include <stdio.h>

#include "hw_memmap.h"
#include "hw_types.h"
#include "hw_sysctl.h"
#include "sysctl.h"
#include "gpio.h"

#include "FreeRTOS.h"
#include "task.h"
#include "userinput.h"
#include "ultrasoundin.h"
#include "oled.h"

#define INPUTTASK_STACK_SIZE configMINIMAL_STACK_SIZE + 10
#define INPUTTASK_PRIORITY tskIDLE_PRIORITY

#define UP_BUTTON GPIO_PIN_3
#define DOWN_BUTTON GPIO_PIN_4
#define LEFT_BUTTON GPIO_PIN_5
#define RIGHT_BUTTON GPIO_PIN_6
#define SELECT_BUTTON GPIO_PIN_7

#define USER_PORT GPIO_PORTG_BASE
#define USER_BUTTONS (UP_BUTTON | DOWN_BUTTON | LEFT_BUTTON | RIGHT_BUTTON | SELECT_BUTTON)

typedef enum {
    CONTINUOUS,
    SINGLE,
} ui_mode;


static int buttonPress(unsigned char pin){
    return GPIOPinRead(USER_PORT, pin) == 0; //Active Low
}


static uint8_t triggerMode = CONTINUOUS; //
static uint8_t singleClick = 1; //Rising edge detection

static void UserInputTask(void *pvParameters){
    OLEDTextMessage xMessage = { "Triggering", 1, 0};
    char messageString[100] = {'\0'};

    xMessage.pcMessage = "Ready    ";
    xQueueSend(OLEDTextQueue, &xMessage, 10);

    while(1){
        if (buttonPress(SELECT_BUTTON) != 0){
            triggerMode = SINGLE;
            if (singleClick == 0){
                xSemaphoreGive(triggerUltrasoundIn);
                singleClick = 1;

                xMessage.pcMessage = "Triggered    ";
                xQueueSend(OLEDTextQueue, &xMessage, 10);
            } else {
                xMessage.pcMessage = "Ready    ";
                xQueueSend(OLEDTextQueue, &xMessage, 10);
            }
        } else if (singleClick == 1){
            singleClick = 0;
            if (triggerMode == SINGLE){
                xMessage.pcMessage = "Ready    ";
                xQueueSend(OLEDTextQueue, &xMessage, 10);
            } else {
                xMessage.pcMessage = "Triggered    ";
                xQueueSend(OLEDTextQueue, &xMessage, 10);
            }
        }
        if (buttonPress(UP_BUTTON) != 0){
            xMessage.pcMessage = "Triggered    ";
            xQueueSend(OLEDTextQueue, &xMessage, 10);
            triggerMode = CONTINUOUS;
        }
        if (triggerMode == CONTINUOUS){
            xSemaphoreGive(triggerUltrasoundIn);
        }
    }
}



void initButtons(void){
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOG);
    GPIOPinTypeGPIOInput(USER_PORT, (USER_BUTTONS));
    GPIOPadConfigSet(
        USER_PORT, USER_BUTTONS, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
}


void UserInputInit(void){
    initButtons();
    xTaskCreate( UserInputTask, ( signed char * ) "InputTask", INPUTTASK_STACK_SIZE, NULL, INPUTTASK_PRIORITY, ( xTaskHandle * ) NULL );
}
