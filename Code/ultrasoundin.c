#include <stdio.h>

//Hardware Includes
#include "hw_memmap.h"
#include "hw_types.h"
#include "sysctl.h"
#include "gpio.h"
#include "adc.h"
#include "pwm.h"

//RTOS Includes
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

//Project Includes
#include "ultrasoundprocessor.h"
#include "ultrasoundin.h"
#include "config.h"
#include "globals.h"


#define ULTRASOUND_MAX_RATE 250 / portTICK_RATE_MS
#define ULTRASOUND_HIGH_TIME 44  //A fudge factor of NOP"s to delay for 10uS

#define ULTRAINTASK_STACK_SIZE configMINIMAL_STACK_SIZE + 100
#define ULTRAINTASK_PRIORITY tskIDLE_PRIORITY
#define LEFT_CHANNEL ADC_CTL_CH1
#define RIGHT_CHANNEL ADC_CTL_CH3

#define DEBUG_PIN_PORT GPIO_PORTF_BASE
#define DEBUG_PIN_PIN GPIO_PIN_7

#define ULTRASOUND_TRIGGER_PIN_PORT GPIO_PORTD_BASE
#define ULTRASOUND_TRIGGER_PIN_PIN GPIO_PIN_1

#define USE_DEBUG_PIN

void stopADCSampling(void){
    PWMGenDisable(PWM_BASE, PWM_GEN_2);
}

void startADCSampling(void){
    currentWriteLocation = 0;
    resetUltrasoundProcessing();
    PWMGenEnable(PWM_BASE, PWM_GEN_2);
}

//------------------  GPIO PINs --------------------------

static void initGPIOPins(void){
    /* Initializes the GPIO used in the ultrasound module */
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    GPIOPinTypeGPIOOutput(ULTRASOUND_TRIGGER_PIN_PORT, ULTRASOUND_TRIGGER_PIN_PIN);

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    GPIOPinTypeGPIOOutput(DEBUG_PIN_PORT, DEBUG_PIN_PIN);
}
static void turnDebugPinOn(void){
    //Turns on a pin used to check the duration of the ADC interrupt
    GPIOPinWrite(DEBUG_PIN_PORT, DEBUG_PIN_PIN, 0xFF);
}

static void turnDebugPinOff(void){
    //Turns off the pin used to check the duration of the ADC interrupt
    GPIOPinWrite(DEBUG_PIN_PORT, DEBUG_PIN_PIN, 0x00);
}

static void turnUltrasoundTriggerOn(void){
    //Turns on the pin used to trigger the ultrasound
    GPIOPinWrite(ULTRASOUND_TRIGGER_PIN_PORT,
        (ULTRASOUND_TRIGGER_PIN_PIN),
        0xFF);
}

static void turnUltrasoundTriggerOff(void){
    //Turns off the pin used to trigger the ultrasound
    GPIOPinWrite(ULTRASOUND_TRIGGER_PIN_PORT,
            (ULTRASOUND_TRIGGER_PIN_PIN),
            0x00);
}

static void triggerUltrasound(void){
    //Drivers the trigger pin to send out an ultrasound pulse
    turnUltrasoundTriggerOn();
    for (unsigned int i=0; i < ULTRASOUND_HIGH_TIME; i++){
        //Delay for 10uS the hard way
        asm("nop");
    }
    turnUltrasoundTriggerOff();
    startADCSampling();
}

// ---------------------- ADC ----------------------------
static uint32_t raw_adc[2] = {0};

void adcInterupt(void){
    //Runs when the ADC has finished a conversion
#ifdef USE_DEBUG_PIN
    turnDebugPinOn();
#endif

    ADCIntClear(ADC_BASE, 0);
    if (currentWriteLocation < TOTAL_BUFFER_SIZE){
        ADCSequenceDataGet(ADC_BASE, 0, raw_adc);
        leftRawBuffer[currentWriteLocation] = (uint8_t)(raw_adc[0] >> 2);
        rightRawBuffer[currentWriteLocation] = (uint8_t)(raw_adc[1] >> 2);

        currentWriteLocation += 1;
    } else {
        stopADCSampling();
    }

#ifdef USE_DEBUG_PIN
    turnDebugPinOff();
#endif
}

static void initADCTrigger(void){
    /* Enables a PWM generator to trigger the ADC at regular intervals */
    unsigned int period = 0;
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM);
    SysCtlPWMClockSet(SYSCTL_PWMDIV_4);
    period = SysCtlClockGet() / 4 / SAMPLE_RATE / 1000;

    //Configure PWM generator
    PWMGenConfigure(PWM_BASE, PWM_GEN_2, PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC);
    PWMGenPeriodSet(PWM_BASE, PWM_GEN_2, period);
    PWMPulseWidthSet(PWM_BASE, PWM_OUT_4, period/2);
    PWMOutputState(PWM_BASE, (PWM_OUT_4_BIT), true);
    //PWMGenEnable(PWM_BASE, PWM_GEN_2);

    //Make it trigger the ADC
    PWMGenIntTrigEnable(PWM_BASE, PWM_GEN_2, PWM_TR_CNT_AD);

    //Enable the pin to debug the PWM with
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_2);

}

static void initADC(void){
    /* Initialized the ADC to sample from the ultrasound sensor */
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC);
    SysCtlADCSpeedSet(SYSCTL_ADCSPEED_1MSPS); //Max sample rate

    //Configure the sample sequence
    ADCSequenceEnable(ADC_BASE, 0);
    ADCSequenceConfigure(ADC_BASE, 0, ADC_TRIGGER_PWM2, 0); //Trigger on PWM

    ADCSequenceStepConfigure(ADC_BASE, 0, 0, RIGHT_CHANNEL);
    ADCSequenceStepConfigure(ADC_BASE, 0, 1,
        ADC_CTL_IE | ADC_CTL_END | LEFT_CHANNEL); //trigger interrupt after sampling channel 1


    //Enable an interrupt on adc sequence 0 conversion complete
    ADCIntRegister(ADC_BASE, 0, adcInterupt);
    ADCIntEnable(ADC_BASE, 0);
}



// ------------------- The FREE RTOS task that handles triggering

static void UltrasoundInTask(void *pvParameters){
    //This task sends a message to the screen every few seconds
    portTickType xLastWakeTime;

    while(1){


        if (xSemaphoreTake( triggerUltrasoundIn, portMAX_DELAY ) == pdPASS){
            triggerUltrasound();
            vTaskDelayUntil( &xLastWakeTime, ULTRASOUND_MAX_RATE );
            xLastWakeTime = xTaskGetTickCount();
        }
    }
}


void UltrasoundInInit(void){
    vSemaphoreCreateBinary( triggerUltrasoundIn );
    xTaskCreate( UltrasoundInTask, ( signed char * ) "UltrasoundIn",ULTRAINTASK_STACK_SIZE, NULL, ULTRAINTASK_PRIORITY, ( xTaskHandle * ) NULL );

    initGPIOPins();
    initADC();
    initADCTrigger();
}
