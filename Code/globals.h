#ifndef GLOBALS_H
#define GLOBALS_H

#include "config.h"
#include <stdio.h>

extern uint8_t leftRawBuffer[TOTAL_BUFFER_SIZE];  //Buffers for storing raw ADC data
extern uint8_t rightRawBuffer[TOTAL_BUFFER_SIZE]; //Do not access these arrays
extern uint16_t currentWriteLocation;             //Beyond this variable
#endif
